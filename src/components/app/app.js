import React, { Component } from 'react';

import {
    Route,
    Switch,
    Redirect,
    withRouter
} from "react-router-dom"

import './app.scss';

import Home from '../pages/home/home'
import Coffee from '../pages/coffee/coffee'
import CoffeeItem from '../pages/coffee_item/coffee_item';
import Goods from '../pages/goods/goods';

class App extends Component {   
    render() {
        const { history } = this.props

        return (
            <div className="App">
                <Switch>
                    <Route history={history} path='/home' component={Home} />
                    <Route history={history} path='/coffee' component={Coffee} />
                    <Route history={history} path='/coffee_page' component={CoffeeItem} />
                    <Route history={history} path='/coffee_goods' component={Goods} />
                    <Redirect from='*' to='/home' />
                </Switch>
            </div>
        );
    }
}

export default withRouter(App)