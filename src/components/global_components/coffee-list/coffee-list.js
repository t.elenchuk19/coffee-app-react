import { Component } from 'react';
import CoffeeListItem from '../coffeeListItem/coffeeListItem';

import "./coffee-list.scss";

class CoffeeList extends Component {
  constructor(props) {
    super(props);
    this.state = { // нельзя напрямую менять state
      data: props.data,
    }
    console.log(this.state);
  }

  render() {
    //console.log(this.props.filter, this.props.filter === "All");
    const elements = this.state.data
      .filter((item) => this.props.isRise ? item.rise : true)
      .filter((item) => this.props.searchData ? item.name.toLowerCase().includes(this.props.searchData) : true)
      .filter((item) => this.props.filter === "All" ? true : this.props.filter === item.country ? true : false)
      .map(item => {
      const { id, ...itemProps } = item;
      return (
        <CoffeeListItem pg={this.props.pg}
          key={id}
          
          {...itemProps} />
      )
    })

    return (
      <ul className="coffee-list">
        {elements}
      </ul>
    )
    }
}

export default CoffeeList;