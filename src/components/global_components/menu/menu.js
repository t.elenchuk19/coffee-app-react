import { Component } from 'react';

import "./menu.scss";

class Menu extends Component {


    render() {
        return (
            <nav className="menu" >
                <ul className="menu-list">
                    <li className={"menu-list__item" + ' ' + this.props.itemType}>
                        <a href="">Coffee house</a></li>
                    <li className="menu-list__item">
                        <a href="">Our coffee</a></li>
                    <li className="menu-list__item">
                        <a href="">For your pleasure</a></li>
                </ul>
            </nav>
        )
    }
}

export default Menu;