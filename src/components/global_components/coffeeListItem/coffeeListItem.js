import './coffeeListItem.scss';

const CoffeeListItem = (props) => {
    const { name, weight, country, prise, img} = props;
  
    return (
        <li className="coffe-list__item item">
            <div className={"item-inner " + props.pg}>
                <img src={img} alt="product img" />
                <div className="item-inner__info info">
                    <h3 className="info__title">{name}<span>{weight}</span> kg</h3>
                    <div className="info__country">{country}</div>
                    <div className="info__price"><span>{prise}</span>$</div>
                </div>
            </div>
        </li>
    )
}

export default CoffeeListItem;