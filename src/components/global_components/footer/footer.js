import Divider from '../divider/divider';
import Menu from '../../global_components/menu/menu';

import "./footer.scss";
//import '../menu/menu.scss';

const Footer = () => {
    return (
        <footer className="footer">
            <Menu itemType="bottom"/>
            <Divider color="dark"/>
        </footer>
    )
}

export default Footer;