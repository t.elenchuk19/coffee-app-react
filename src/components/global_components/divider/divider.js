import { Component } from 'react';
import './divider.scss';
import logoWhite from './white_logo.png';
import logoBlack from './dark_logo.png';


class Divider extends Component {

    imgSrc = () => {
        return this.props.color === 'white' ? logoWhite : logoBlack;
    }

    render() {
        return (
            <div className={'divider' + ' ' + this.props.color}><img src={this.imgSrc()} alt="divider" /></div>
        )
   }
}

export default Divider;