import { Component } from 'react'
import Menu from '../../global_components/menu/menu';

import "./header.scss";
import Divider from '../divider/divider';


class Header extends Component{
    render() {
        return (
            <header className="header">
                <div className="container container-full">
                    <Menu itemType='top' />
                    <div className="header-wrapper">
                        <h1>Everything You Love About Coffee</h1>
                        <Divider color='white' />
                        <div className="header-wrapper__descr">We makes every day full of energy and taste</div>
                        <div className="header-wrapper__descr">Want to try our beans?</div>
                        <button type="button"
                            className="header-wrapper__button">
                            More</button>
                    </div>
                </div>
            </header>
        )
    }
}

export default Header;