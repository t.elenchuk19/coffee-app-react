import { Component } from 'react';
import Divider from "../divider/divider";
import "./about_our.scss";


class AboutOur extends Component {
    render() {
        return (
            <section className="about-our">
                    <div className="about-our-wrapper">
                        <div className="about-our-wrapper__img">
                            <img src={this.props.img} alt="" />
                        </div>
                        <div className="about-our-wrapper__info info">
                            <h2 className="info-title">About our {this.props.titleName}</h2>
                            <Divider color='dark' />
                            <p className="info-descr">Extremity sweetness difficult behaviour he of. On disposal of as landlord horrible.</p>
                            <p className="info-descr">Afraid at highly months do things on at. Situation recommend objection do intention
                                so questions.
                                As greatly removed calling pleased improve an. Last ask him cold feel
                                met spot shy want. Children me laughing we prospect answered followed. At it went
                                is song that held help face.</p>
                        </div>
                    </div>
            </section>
        )
    }
}

export default AboutOur;