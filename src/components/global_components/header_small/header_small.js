import { Component } from 'react'
import Menu from '../menu/menu';

import './header_small.scss';

class HeaderSmall extends Component {
    render() {
        return (
            <header className={'header-small' + ' ' + this.props.bg}>
                <div className="container">
                    <div className='header-small-wrapper'>
                        <Menu itemType='top'/>
                        <h1>Our Coffee</h1>
                    </div>
                </div>
            </header>
        )
    }
}

export default HeaderSmall;