import HeaderSmall from '../../global_components/header_small/header_small';
import Divider from '../../global_components/divider/divider';
import Footer from '../../global_components/footer/footer';

import './coffee_item.scss';
import img from './aromistico_img.jpeg';

const CoffeeItem = () => {
    return (
        <div>
            <HeaderSmall bg="coffee" />
            <section className="coffee_item container">
                <div className="coffee_item-wrapper">
                    <div className="coffee_item-wrapper__img">
                        <img src={img} alt="" />
                    </div>
                    <div className="coffee_item-wrapper__info info">
                        <h2 className="info-title">About it</h2>
                        <Divider color='dark' />
                        <div className="info-wrapper">
                            <div className="info-wrapper_inner">Country:<span> Brasil</span> </div>
                            <div className="info-wrapper_inner">Description:<span> Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</span></div>
                            <div className="info-wrapper_price">Price:<span> 16.99$</span></div>
                        </div>
                    </div>
                </div>
            </section>
            <Footer />
    </div>
);
}

export default CoffeeItem;