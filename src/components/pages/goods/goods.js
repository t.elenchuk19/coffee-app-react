import { Component } from 'react';
import axios from 'axios';
import HeaderSmall from "../../global_components/header_small/header_small";
import AboutOur from "../../global_components/about-our/about_our";
import CoffeeList from "../../global_components/coffee-list/coffee-list";
import Footer from "../../global_components/footer/footer";

import img from "./coffee_cup.jpeg";


class Goods extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            filter: 'All'
        }
    }

    //Function which is called when the component loads for the first time
    componentDidMount() {
        this.getCustomerData()
    }


    //Function to get the Customer Data from json
    getCustomerData() {
        axios.get('assets/data.json').then(response => {
            this.setState({ data: response.data })
            //console.log(response, this.state.data);
        })
    };

    render() {
        if (!this.state.data.length) {
            return (
                <div style={{ textAlign: "center" }}>Loading...</div>
            )
        }
        return (
            <div>
                <HeaderSmall bg="goods" />
                <main className="main-coffee container">
                    <AboutOur img={img}
                        titleName='goods' />
                    <CoffeeList
                        pg="pg-white"
                        isRise={false}
                        data={this.state.data}
                        filter={this.state.filter}
                    />
                </main>
                <Footer />
            </div>
        )
   }
}

export default Goods;