import { Component } from 'react';
import './search.scss';

class SearchPanel extends Component{
    constructor(props) {
        super(props);
        this.state = {
            term: ''
        }
    }

    onUpdateSearch = (e) => {
        const term = e.target.value;
        this.setState({ term }); //  установка локального состояния, которое выше в this.state
        this.props.onUpdateSearchChild(term); //когда установили лок.состояние,пробрасываем его на верх через метод, который пришел через props
    }


    render() {
        return (
            <div className="search-panel">
                <div className="search-panel-text">Lookiing for</div>
                <input type="text"
                    tabIndex="0"
                    className="search-panel-input"
                    placeholder="start typing here..."
                    value={this.state.term}
                    onChange={ this.onUpdateSearch}/>
            </div>
        )
   }
}

export default SearchPanel;