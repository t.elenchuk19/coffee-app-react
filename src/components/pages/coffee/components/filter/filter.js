import "./filter.scss"

const Filter = (props) => {
    const buttonsData = [
        { name: 'All', label: 'All' },
        { name: 'Brazil', label: 'Brazil' },
        { name: 'Kenya', label: 'Kenya' },
        { name: 'Columbia', label: 'Columbia' }
    ];

        const buttons = buttonsData.map(({ name, label }) => {
            const active = props.filter === name;
            const clazz = active ? 'btn-light' : 'btn-outline-light';
            return (
                <button type="button"
                    className={`filter-btn-group__item ${clazz}`}
                    key={name}
                    onClick={() => props.onFilterSelect(name)}>
                    {label}
                </button>
            )
        })

    return (
        <div className="filter">
            <div className="filter-text">Or filter</div>
            <div className="filter-btn-group">
                {buttons}
            </div>
       </div>
    )
}

export default Filter;