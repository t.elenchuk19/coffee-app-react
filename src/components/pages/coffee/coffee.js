import { Component } from 'react';
import HeaderSmall from "../../global_components/header_small/header_small";
import Footer from '../../global_components/footer/footer';
import AboutOur from "../../global_components/about-our/about_our";
import SearchPanel from "./components/search/search";
import Filter from "./components/filter/filter";
import CoffeeList from "../../global_components/coffee-list/coffee-list";
import axios from 'axios';

import img from './girl.jpeg';
import "./coffee.scss";


class Coffee extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            term: '',
            filter: 'All'
        }
    }

    //Function which is called when the component loads for the first time
    componentDidMount() {
        this.getCustomerData()
    }


    //Function to get the Customer Data from json
    getCustomerData() {
        axios.get('assets/data.json').then(response => {
            this.setState({ data: response.data })
        })
    };

    onUpdateSearch = (e) => {
        this.setState({ term: e }); //  установка локального состояния, которое выше в this.state
    }

    onFilterSelect = (filter) => {
        this.setState({ filter });
    }


    render() {
        const { filter } = this.state;
        if (!this.state.data.length) {
            return (
                <div style={{ textAlign: "center" }}>Loading...</div>
            )
        }
        return (
            <div className="">
                <HeaderSmall bg="coffee" />
                <main className="main-coffee container">
                    <AboutOur
                        img={img}
                        titleName='beans' />
                    <section className="main-coffee-actions">
                        <SearchPanel onUpdateSearchChild={this.onUpdateSearch} />
                        <Filter onFilterSelect={this.onFilterSelect}/>
                    </section>
                    <CoffeeList pg="pg-white"
                        data={this.state.data}
                        isRise={false}
                        searchData={this.state.term}
                        filter={filter}
                    />
                </main>
                <Footer />
            </div>
        )
    }
}

export default Coffee;