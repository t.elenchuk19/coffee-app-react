import { Component } from 'react';
import CoffeeList from "../../../global_components/coffee-list/coffee-list";
import Loading from '../../../global_components/loading/loading';
import axios from 'axios';

import "./best.scss";

class Best extends Component {
    constructor(props) {
        super(props);
        this.state = {// нельзя напрямую менять state
            data: [],
            filter: 'All'
        }
    }
    
    //Function which is called when the component loads for the first time
    componentDidMount() {
        this.getCustomerData()
    }


    //Function to get the Customer Data from json
    getCustomerData() {
        axios.get('assets/data.json').then(response => {
            this.setState({ data: response.data })
        })
    };

    render() {
        if (!this.state.data.length) {
            return (
                // <Loading/>
                <div style={{textAlign: "center"}}>Loading...</div>
            )
        }
        return (
            <section className='best'>
                <div className="container">
                    <h2 className='best-title'>Our best</h2>
                    <CoffeeList pg="pg-home"
                        data={this.state.data}
                        isRise={true}
                        filter={this.state.filter}
                    />
                </div>
            </section>
        )
   }
}

export default Best;