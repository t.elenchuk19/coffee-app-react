import Header from '../../global_components/header/header';
import  AboutUs from "./section_about-us/about_us";
import Best from './section_best/best';
import Footer from '../../global_components/footer/footer'

import "./home.scss";

const Home = () => {
    return (
      <div className="home" >
        <Header />
        <AboutUs />
        <Best />
        <Footer />
        </div>
    )
}

export default Home;